const express = require('express');
const app = express();

const TEST_VAR = process.env.TEST_VAR;

app.use((req, res) => {
    console.log('Handling request:', TEST_VAR, req.path);
    res.send('Hello World 5! ' + TEST_VAR + ' ' + req.path);
});

app.listen(process.env.PORT || 3000, () => {
  console.log('Example app listening on port 3000!');
});